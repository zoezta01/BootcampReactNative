function range(startNum, finishNum)
{
	var numbers = []; // hold all the numbers within the range
	if(startNum > finishNum)
	{
		for(var i = startNum; i >= finishNum; i -= 1)
		{
			numbers.push(i); // add the number to the array of numbers
		}
    } 
    else if(startNum == null || finishNum == null)
        {
            return -1;
        }
    else
	{
		for(var i = startNum; i <= finishNum; i += 1)
		{
			numbers.push(i); // add the number to the array of numbers
		}
	}
    return numbers;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
