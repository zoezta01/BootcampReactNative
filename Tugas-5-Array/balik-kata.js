function balikKata(kata){
    var balik = "";
    for (var i = 0; i < kata.length; i++){
        balik = kata[i] + balik;
    }
    return balik;
}
console.log(balikKata("1234567890-")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
