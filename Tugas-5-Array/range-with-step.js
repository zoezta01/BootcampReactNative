function rangeWithStep(startNum, finishNum, step)
{
	var numbers = []; // hold all the numbers within the range
	if(startNum > finishNum)
	{
		for(var i = startNum; i >= finishNum; i -= step)
		{
			numbers.push(i); // add the number to the array of numbers
		}
    } 
    else
	{
		for(var i = startNum; i <= finishNum; i += step)
		{
			numbers.push(i); // add the number to the array of numbers
		}
	}
    return numbers;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
