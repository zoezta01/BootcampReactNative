function arrayToObject(arr){   
    if (arr.length <= 0 ){
        return console.log("")
    }
    for (var i=0; i<arr.length; i++){
        var newObject = {}
        var birthYear = arr[i][3];
        var now = new Date().getFullYear()
        var newAge;
        if (birthYear && now - birthYear > 0){
            newAge = now -birthYear
        }else{
            newAge = " Invalid Birth Year"
        }
        newObject.firstName = arr[i][0]
        newObject.lastName = arr[1][1]
        newObject.gender = arr[i][2]
        newObject.age = newAge

        var consoleText = (i+1) +'. '+ newObject.firstName +' '+ newObject.lastName + ' : '

        console.log(consoleText)
        console.log(newObject)
    }
}
console.log("======soal 1======")
var people = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
arrayToObject(people)

function shoppingTime(memberId, money){
    if (!memberId){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if(money < 50000){
        return 'Mohon maaf, uang tidak cukup'
    }else{
        var newObject = {}
        var changeMoney = money;
        var purcaseList = [];
        var sepatu = "sepatu Stacattu"
        var bajuZoro = "Zoro"
        var bajuHN = "H&N"
        var sweater = "Uniklooh"
        var casing = "Casing HP"
        
        var chek = 0;
        for(var i = 0; changeMoney >= 50000 && chek == 0; i++){
            if(changeMoney >= 1500000){
                purcaseList.push(sepatu)
                changeMoney -= 1500000
            }else if(changeMoney >= 500000){
                purcaseList.push(bajuZoro)
                changeMoney -= 500000
            }else if(changeMoney >= 250000){
                purcaseList.push(bajuHN)
                changeMoney -= 250000
            }else if(changeMoney >= 175000){
                purcaseList.push(sweater)
                changeMoney -= 175000
            }else if(changeMoney >= 50000){
                for(var j = 0; j <= purcaseList; j++){
                    if(purcaseList[j] == casing){
                        chek += 1
                    }
                }if(chek == 0){
                    purcaseList.push(casing)
                    changeMoney -= 50000
                }else{
                    purcaseList.push(casing)
                    changeMoney -= 50000
                }
            }
        }
        newObject.memberId = memberId
        newObject.money = money
        newObject.purcaseList = purcaseList
        newObject.changeMoney = changeMoney
        return newObject
    }
}
console.log("======NO 2======")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

function naikAngkot(listPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var output = []
    if(listPenumpang.length <= 0){
        return []
    }

    for(var i = 0; i<listPenumpang.length; i++){
        var newOutput = {}
        var asal = listPenumpang[i][1]
        var tujuan = listPenumpang[i][2]
        
        var indexAsal;
        var indexTujuan;

        for(var j = 0; j<rute.length; j++){
            if(rute[j] == asal){
                indexAsal = j
            }else if(rute[j] == tujuan){
                indexTujuan = j
            }
        }

        var bayar = (indexTujuan - indexAsal) * 2000

        newOutput.penumpang = listPenumpang[i][0]
        newOutput.naikDari = asal
        newOutput.tujuan = tujuan
        newOutput.bayar = bayar

        output.push(newOutput)
    }
    return output
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));